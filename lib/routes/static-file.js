'use strict';
const path = require ('path')

module.exports = {
    method: 'GET',
    path: '/static',
    options: {
        handler: async (request, h) => {
            return h.file (path.join (__dirname, '../../public/static.html'))
        }
    }
};
